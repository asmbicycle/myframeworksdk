Pod::Spec.new do |s|
    s.name         = "MyFrameworkAS"
    s.version      = "1.0.0"
    s.summary      = "A brief description of MyFramework project."
    s.description  = "An extended description of MyFramework project."
    s.homepage     = "http://your.homepage/here"
    s.license = { :type => "Copyright", :text => <<-LICENSE
                   Copyright 2018
                   Permission is granted to...
                  LICENSE
                }
    s.author             = "user.email@mail.ru"
    s.source       = { :git => "https://asmbicycle@bitbucket.org/asmbicycle/myframeworksdk.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "MyFramework.xcframework"
    s.platform = :ios
    s.swift_version = "4.2"
    s.ios.deployment_target  = "12.0"
end
